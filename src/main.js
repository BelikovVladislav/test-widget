import Vue from 'vue'
import VueBootstrap from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import TableWidget from './components/Table';
import VueCustomElement from 'vue-custom-element';

Vue.config.productionTip = false;
Vue.use(VueBootstrap);
Vue.use(VueCustomElement);
Vue.customElement('table-widget', TableWidget);
